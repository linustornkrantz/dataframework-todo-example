package com.prettybyte.todo

import com.prettybyte.clerk.Context
import com.prettybyte.clerk.IdempotenceKey
import com.prettybyte.clerk.SystemIdentity
import com.prettybyte.clerk.event.Command
import com.prettybyte.clerk.event.CommandOptions

import com.prettybyte.todo.data.*
import com.prettybyte.todo.data.Languages.English
import com.prettybyte.todo.data.todo.CreateTodoParams
import com.prettybyte.todo.data.todo.TodoEvents
import com.prettybyte.todo.data.user.CreateUserParams
import com.prettybyte.todo.data.user.UserEvents

suspend fun createSampleData() {
    clerk.handle(
        Command(
            event = UserEvents.`Create user`.toEvent(),
            reference = null,
            eventParamsObject = CreateUserParams(
                name = UserName("Leela"),
                isAdmin = IsAdminValue(true),
                language = Language(English)
            ),
        ),
        Context(SystemIdentity),
        CommandOptions(IdempotenceKey.create())
    ).orThrow()

    clerk.handle(
        Command(
            event = UserEvents.`Create user`.toEvent(),
            reference = null,
            eventParamsObject = CreateUserParams(
                name = UserName("Zoidberg"),
                isAdmin = IsAdminValue(false),
                language = Language(English)
            ),
        ),
        Context(SystemIdentity),
        CommandOptions(IdempotenceKey.create())
    ).orThrow()

    val anyUser = clerk.read(Context(SystemIdentity)) { list(views.users.all).first() }

    clerk.handle(
        Command(
            event = TodoEvents.`Create todo`.toEvent(),
            reference = null,
            eventParamsObject = CreateTodoParams(
                title = Title("Something must be done"),
                body = Body("We should do something about it!"),
                owner = anyUser.id
            ),
        ),
        Context(SystemIdentity),
        CommandOptions(IdempotenceKey.create())
    ).orThrow()
}
