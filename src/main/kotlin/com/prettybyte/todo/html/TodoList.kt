package com.prettybyte.todo.html

import com.prettybyte.clerk.*
import com.prettybyte.clerk.event.Command
import com.prettybyte.clerk.event.CommandOptions
import com.prettybyte.clerk.misc.EventParameters
import com.prettybyte.todo.context
import com.prettybyte.todo.clerk
import com.prettybyte.todo.data.Body
import com.prettybyte.todo.data.Title
import com.prettybyte.todo.data.todo.CreateTodoParams
import com.prettybyte.todo.data.todo.Todo
import com.prettybyte.todo.data.todo.TodoEvents
import com.prettybyte.todo.data.user.User
import com.prettybyte.webutils.EventFormTemplate
import com.prettybyte.webutils.ParseResult.*
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.html.*
import io.ktor.server.response.*
import kotlinx.html.*

suspend fun renderGetTodoList(call: ApplicationCall) {
    val context = call.context()

    clerk.readSuspend(context) {
        val createTodoFormDefaultValues = CreateTodoParams(
            title = Title(""),
            body = Body(""),
            owner = findOwner(context)
        )
        val createTodoForm = createTodoFormTemplate.build(
            call,
            createTodoFormDefaultValues,
            reader = this,
            referenceSelects = mapOf(CreateTodoParams::owner to views.users.all)
        )
        call.respondHtml {
            head {
                link(rel = "stylesheet", type = "text/css", href = WEB_APP_CSS)
            }
            body {
                h1 { +"Todos" }
                table {
                    tr {
                        th { +"Title" }
                        th { +"State" }
                    }
                    list(views.todos.all).forEach { todo ->
                        tr {
                            td { a(href = "/todos/${todo.id}") { +todo.props.title.toString() } }
                            td { +todo.state.toString() }
                        }
                    }
                }
                if (clerk.config.getPossibleVoidEvents(context, Todo::class)
                        .any() { it == TodoEvents.`Create todo`.toEvent() }
                ) {
                    hr()
                    h2 { +"Create new todo" }
                    createTodoForm.render(this)
                }
            }
        }
    }
}

private fun findOwner(context: Context): Reference<User> {
    return (context.actor as? ModelIdentity<User>?)?.model?.id ?: throw IllegalArgumentException()
}

suspend fun renderPostTodoList(call: ApplicationCall) {
    when (val result = createTodoFormTemplate.parse(call, call.context())) {
        is Invalid -> EventFormTemplate.respondInvalid(result, call)
        is DryRun -> call.respond(HttpStatusCode.OK)
        is Parsed -> makeCreateEvent(result, call)
    }
}

private suspend fun makeCreateEvent(parsed: Parsed<CreateTodoParams>, call: ApplicationCall) {
    val eventMessage = when (val result = clerk.handle(
        Command(
            event = TodoEvents.`Create todo`.toEvent(),
            reference = null,
            eventParamsObject = parsed.params,
        ),
        call.context(),
        CommandOptions(parsed.idempotenceKey)
    )) {
        is CommandResult.Failure -> "Sorry, the event failed (${result.problem})"
        is CommandResult.Success -> "The todo has been created!"
    }
    call.respondHtml {
        head {
            link(rel = "stylesheet", type = "text/css", href = WEB_APP_CSS)
        }
        body {
            h1 { +eventMessage }
            a(href = "/todos") { +"Back" }
        }
    }
}

val createTodoFormTemplate = EventFormTemplate(EventWithParameters(
    event = TodoEvents.`Create todo`.toEvent(),
    parameters = EventParameters(CreateTodoParams::class)
), clerk,"/todos") {
    text(CreateTodoParams::title)
    text(CreateTodoParams::body)
    selectReference(CreateTodoParams::owner)
}
