package com.prettybyte.todo.html

import com.prettybyte.clerk.*
import com.prettybyte.clerk.event.Command
import com.prettybyte.clerk.event.CommandOptions
import com.prettybyte.clerk.misc.EventParameters
import com.prettybyte.todo.clerk
import com.prettybyte.todo.context
import com.prettybyte.todo.data.HoursSpent
import com.prettybyte.todo.data.todo.*
import com.prettybyte.webutils.EventFormTemplate
import com.prettybyte.webutils.ParseResult.*
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.html.*
import io.ktor.server.response.*
import kotlinx.html.*
import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.format.DateTimeFormatter

private val dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss")

suspend fun renderTodoDetails(call: ApplicationCall) {
    val context = call.context()
    val id = call.parameters["id"] ?: throw IllegalStateException()
    val reference = Reference.from<Todo>(id)

    val possibleEvents = clerk.read(context) { getPossibleEvents(reference) }

    val (todo, ownerName, hoursForm) = clerk.read(context) {
        val todo = get(reference)
        val ownerName = get(todo.props.owner).props.name.toString()
        val hoursFormDefaultValue = SetHoursParams(todo.props.hoursSpent ?: HoursSpent(0))
        val hoursForm =
            hoursFormBuilder.build(call, hoursFormDefaultValue, this, path = "/todos/${todo.id}?command=hours")
        Triple(todo, ownerName, hoursForm)
    }

    call.respondHtml {
        head {
            link(rel = "stylesheet", type = "text/css", href = WEB_APP_CSS)
        }
        body {
            h1 { +"Todo details" }
            a(href = "/todos") { +"Back" }
            table {
                tr {
                    td { +"ID" }
                    td { +todo.id.toString() }
                }
                tr {
                    td { +"State" }
                    td { +todo.state.toString() }
                }
                tr {
                    td { +"Created" }
                    td { +toNiceString(todo.createdAt) }
                }
                tr {
                    td { +"Last modified" }
                    td { +toNiceString(todo.lastModifiedAt) }
                }
                tr {
                    td { +"Title" }
                    td { +todo.props.title.toString() }
                }
                tr {
                    td { +"Body" }
                    td { +todo.props.body.toString() }
                }
                tr {
                    td { +"Owner" }
                    td { +ownerName }
                }
                tr {
                    td { +"Hours spent" }
                    td { +todo.props.hoursSpent.toString() }
                }
            }

            if (possibleEvents.any { it.eventName == TodoEvents.Update.name }) {
                a(href = "/todos/${todo.id}/edit") { button { +"Edit" } }
            }

            if (possibleEvents.any { it.eventName == TodoEvents.`Set hours`.name }) {
                h2 { +"Set hours" }
                hoursForm.render(this)
            }

            if (possibleEvents.any { it.eventName == TodoEvents.`Mark as done`.name }) {
                form(action = "/todos/${todo.id}?command=done", method = FormMethod.post) {
                    button { +"Mark as done" }
                }
            }

            if (possibleEvents.any { it.eventName == TodoEvents.`Mark as not done`.name }) {
                form(action = "/todos/${todo.id}?command=notdone", method = FormMethod.post) {
                    button { +"Mark as not done" }
                }
            }

            if (possibleEvents.any { it.eventName == TodoEvents.`Delete todo`.name }) {
                form(action = "/todos/${todo.id}?command=delete", method = FormMethod.post) {
                    button { +"Delete" }
                }
            }

        }
    }
}

private fun toNiceString(instant: Instant): String {
    return LocalDateTime.ofInstant(instant, ZoneId.systemDefault()).format(dateTimeFormatter)
}

suspend fun handleTodoCommand(call: ApplicationCall) {
    val command = call.request.queryParameters["command"] ?: throw IllegalArgumentException("Command must be provided")
    when (command) {
        "update" -> {
            when (val result = updateTodoFormBuilder.parse(call, call.context())) {
                is Invalid -> EventFormTemplate.respondInvalid(result, call)
                is DryRun -> call.respond(HttpStatusCode.OK)
                is Parsed -> makeUpdateEvent(result, call)
            }
        }

        "hours" -> {
            when (val result = hoursFormBuilder.parse(call, call.context())) {
                is Invalid -> EventFormTemplate.respondInvalid(result, call)
                is DryRun -> call.respond(HttpStatusCode.OK)
                is Parsed -> makeHoursEvent(result, call)
            }
        }

        "done" -> {
            makeDoneEvent(call)
        }

        "notdone" -> {
            makeNotDoneEvent(call)
        }

        "delete" -> {
            makeDeleteEvent(call)
        }
    }
}

private suspend fun makeUpdateEvent(parsed: Parsed<UpdateTodoParams>, call: ApplicationCall) {
    val id = call.parameters["id"] ?: throw IllegalStateException()
    val eventMessage = when (val result = clerk.handle(
        Command(
            event = TodoEvents.Update.toEvent(),
            eventParamsObject = parsed.params,
            reference = Reference.from(id),
        ),
        call.context(),
        CommandOptions(parsed.idempotenceKey)

    )
    ) {
        is CommandResult.Failure -> "Sorry, the event failed (${result.problem})"
        is CommandResult.Success -> "The todo has been updated!"
    }
    renderResult(call, eventMessage, "/todos/$id")
}

private suspend fun makeHoursEvent(parsed: Parsed<SetHoursParams>, call: ApplicationCall) {
    val id = call.parameters["id"] ?: throw IllegalStateException()
    val eventMessage = when (val result = clerk.handle(
        Command(
            event = TodoEvents.`Set hours`.toEvent(),
            eventParamsObject = parsed.params,
            reference = Reference.from(id),
        ),
        call.context(),
        CommandOptions(parsed.idempotenceKey)
    )) {
        is CommandResult.Failure -> "Sorry, the event failed (${result.problem})"
        is CommandResult.Success -> "The time of the todo has been updated!"
    }
    renderResult(call, eventMessage, "/todos/$id")
}

private suspend fun makeDoneEvent(call: ApplicationCall) {
    val id = call.parameters["id"] ?: throw IllegalStateException()
    val eventMessage = when (val result = clerk.handle(
        Command(
            event = TodoEvents.`Mark as done`.toEvent(),
            reference = Reference.from(id),
        ),
        call.context(),
        CommandOptions(IdempotenceKey.create())      // TODO: which key should we use?
    )) {
        is CommandResult.Failure -> "Sorry, the event failed (${result.problem})"
        is CommandResult.Success -> "The todo has been marked as done!"
    }
    renderResult(call, eventMessage, "/todos/$id")
}

private suspend fun makeNotDoneEvent(call: ApplicationCall) {
    val id = call.parameters["id"] ?: throw IllegalStateException()
    val eventMessage = when (val result = clerk.handle(
        Command(
            event = TodoEvents.`Mark as not done`.toEvent(),
            reference = Reference.from(id),
        ),
        call.context(),
        CommandOptions(IdempotenceKey.create())      // TODO: which key should we use?
    )) {
        is CommandResult.Failure -> "Sorry, the event failed (${result.problem})"
        is CommandResult.Success -> "The todo has been marked as not done!"
    }
    renderResult(call, eventMessage, "/todos/$id")
}

private suspend fun makeDeleteEvent(call: ApplicationCall) {
    val id = call.parameters["id"] ?: throw IllegalStateException()
    val eventMessage = when (val result = clerk.handle(
        Command(
            event = TodoEvents.`Delete todo`.toEvent(),
            reference = Reference.from(id),
        ),
        call.context(),
        CommandOptions(IdempotenceKey.create())      // TODO: which key should we use?
    )) {
        is CommandResult.Failure -> "Sorry, the event failed (${result.problem})"
        is CommandResult.Success -> "The todo has been deleted!"
    }
    renderResult(call, eventMessage, "/")
}

private suspend fun renderResult(call: ApplicationCall, eventMessage: String, backPath: String) {
    call.respondHtml {
        head {
            link(rel = "stylesheet", type = "text/css", href = WEB_APP_CSS)
        }
        body {
            h1 { +"Event result" }
            p { +eventMessage }
            a(href = backPath) { +"Back" }
        }
    }
}

val hoursFormBuilder = EventFormTemplate(
    EventWithParameters(
        event = TodoEvents.`Set hours`.toEvent(),
        parameters = EventParameters(SetHoursParams::class)
    ), clerk, null
) {
    number(SetHoursParams::hours)
}
