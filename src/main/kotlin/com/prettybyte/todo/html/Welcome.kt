package com.prettybyte.todo.html

import io.ktor.server.application.*
import io.ktor.server.html.*
import kotlinx.html.*

const val WEB_APP_CSS = "https://cdn.jsdelivr.net/npm/water.css@2/out/water.css"

suspend fun renderWelcome(call: ApplicationCall) {
    call.respondHtml {
        head {
            link(rel = "stylesheet", type = "text/css", href = WEB_APP_CSS)
        }
        body {
            h1 { +"Todo app using Dataframework" }
            p { +"This todo app demonstrates how you can build with Dataframework." }
            p {
                ul {
                    li {
                        +"The "
                        a(href = "/todos") { +"Web app" }
                        +" (logged in as Zoidberg)"
                    }
                    li {
                        +"Take a look at the generated "
                        a("/admin") { +"Admin UI" }
                    }
                    li {
                        +"You can also interact with the data using "
                        a(href = "/graphiql") { +"GraphQL" }
                        +" (logged in as Zoidberg)"
                    }
                }
            }
            h2 { +"Business logic and rules" }
            p {
                +"The app configures Dataframework with these (and a few more) rules:"
                ul {
                    li { +"Everybody can read everything" }
                    li { +"Everybody can do everything" }
                    li { +"Zoidberg cannot create todos starting with the letter Z on even minutes" }
                    li { +"The Todo title must be between 5-50 characters" }
                    li { +"Title must start with a capital letter" }
                    li { +"Hours spent can only be set to a value between 0-10" }
                    li { +"The event 'Mark as done' will only be accepted if hours spent has been set" }
                    li { +"When a Todo is done, an email job is created" }
                }
                +"Information about which events are allowed in which states can be found on the Documentation page in the Admin UI."
            }
        }
    }
}
