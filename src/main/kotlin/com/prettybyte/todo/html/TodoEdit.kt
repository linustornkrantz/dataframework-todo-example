package com.prettybyte.todo.html

import com.prettybyte.clerk.EventWithParameters
import com.prettybyte.clerk.Model
import com.prettybyte.clerk.Reference
import com.prettybyte.clerk.misc.EventParameters
import com.prettybyte.todo.context
import com.prettybyte.todo.clerk
import com.prettybyte.todo.data.Body
import com.prettybyte.todo.data.todo.CreateTodoParams
import com.prettybyte.todo.data.todo.Todo
import com.prettybyte.todo.data.todo.TodoEvents
import com.prettybyte.todo.data.todo.UpdateTodoParams
import com.prettybyte.webutils.EventFormTemplate
import io.ktor.server.application.*
import io.ktor.server.html.*
import kotlinx.html.body
import kotlinx.html.h2
import kotlinx.html.head
import kotlinx.html.link

suspend fun renderTodoEdit(call: ApplicationCall) {
    val id = call.parameters["id"] ?: throw IllegalStateException()
    val context = call.context()

    clerk.readSuspend(context) {
        val todo: Model<Todo> = get(Reference.from(id))

        val updateFormDefaultValues =
            CreateTodoParams(
                title = todo.props.title,
                body = todo.props.body ?: Body(""),
                owner = todo.props.owner
            )
        val updateTodoForm =
            updateTodoFormBuilder.build(
                call,
                updateFormDefaultValues,
                this,
                path = "/todos/${todo.id}?command=update",
                referenceSelects = mapOf(CreateTodoParams::owner to views.users.all)
            )

        call.respondHtml {
            head {
                link(rel = "stylesheet", type = "text/css", href = WEB_APP_CSS)
            }
            body {
                h2 { +"Update todo" }
                updateTodoForm.render(this)
            }
        }
    }
}

val updateTodoFormBuilder = EventFormTemplate(EventWithParameters(
    event = TodoEvents.Update.toEvent(),
    parameters = EventParameters(UpdateTodoParams::class)),
    clerk) {
    text(UpdateTodoParams::title)
    text(UpdateTodoParams::body)
    selectReference(UpdateTodoParams::owner)
}
