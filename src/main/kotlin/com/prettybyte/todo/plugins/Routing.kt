package com.prettybyte.todo.plugins

import com.expediagroup.graphql.server.ktor.graphQLGetRoute
import com.expediagroup.graphql.server.ktor.graphQLPostRoute
import com.expediagroup.graphql.server.ktor.graphQLSDLRoute
import com.expediagroup.graphql.server.ktor.graphiQLRoute
import com.prettybyte.clerk.Context
import com.prettybyte.clerk.Event
import com.prettybyte.todo.appMicrometerRegistry
import com.prettybyte.todo.context
import com.prettybyte.todo.clerk
import com.prettybyte.todo.html.*
import com.prettybyte.webutils.LowCodeConfig
import com.prettybyte.webutils.LowCodeMain
import io.ktor.server.application.*
import io.ktor.server.response.*
import io.ktor.server.routing.*

fun Application.configureRouting() {
    routing {

        // Metrics
        get("/metrics") { call.respond(appMicrometerRegistry.scrape()) }

        // The auto-generated Admin UI
        apply(lowCodeMain.registerRoutes())

        // GraphQL
        graphQLPostRoute()
        graphQLGetRoute()
        graphiQLRoute()
        graphQLSDLRoute()

        // Routes for the web app
        get("/") { renderWelcome(call) }
        get("/todos") { renderGetTodoList(call) }
        post("/todos") { renderPostTodoList(call) }
        get("/todos/{id}") { renderTodoDetails(call) }
        get("/todos/{id}/edit") { renderTodoEdit(call) }
        post("/todos/{id}") { handleTodoCommand(call) }

    }
}

private val lowCodeMain = LowCodeMain(
    clerk, LowCodeConfig(
        basePath = "/admin",
        contextProvider = ::contextFromCall,
        showOptionalParameters = ::showOptionalParameters,
        cssPath = "https://unpkg.com/sakura.css/css/sakura.css"
    )
)

suspend fun contextFromCall(call: ApplicationCall): Context = call.context()

private fun showOptionalParameters(event: Event): Boolean {
    return false
}
