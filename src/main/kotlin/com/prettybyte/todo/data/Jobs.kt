package com.prettybyte.todo.data

import com.prettybyte.clerk.event.Command
import com.prettybyte.clerk.Model
import com.prettybyte.clerk.actions.Job
import com.prettybyte.clerk.actions.JobContext
import com.prettybyte.clerk.actions.JobResult
import com.prettybyte.todo.data.todo.Todo

class `Notify boss that job is done` : Job<Todo> {
    override suspend fun run(model: Model<Todo>?, command: Command<Todo>, context: JobContext): JobResult {
        // send email to boss here
        return JobResult.Success
    }

}
