package com.prettybyte.todo.data

import com.prettybyte.clerk.Config
import com.prettybyte.clerk.ConfigBuilder
import com.prettybyte.clerk.storage.Persistence
import com.prettybyte.clerk.*
import com.prettybyte.clerk.NegativeAuthorization.*
import com.prettybyte.clerk.PositiveAuthorization.*
import com.prettybyte.clerk.event.Command
import com.prettybyte.clerk.read.Reader
import com.prettybyte.todo.appMicrometerRegistry
import com.prettybyte.todo.data.todo.*
import com.prettybyte.todo.data.user.User
import com.prettybyte.todo.data.user.userStateMachine
import java.time.LocalTime
import java.time.ZoneId

private val views = Views()

fun createConfig(persistence: Persistence): Config<Views> {
    return ConfigBuilder(views).build {
        persistence(persistence)
        managedModels {
            model(User::class, userStateMachine(), views.users)
            model(Todo::class, todoStateMachine(views), views.todos)
        }
        authorizationRules {
            reads {
                positive {
                    rule(::`Everybody can read everything`)
                }
                negative {
                }
            }
            events {
                positive {
                    rule(::`Everybody can do everything`)
                }
                negative {
                    rule(::`Zoidberg cannot create todos starting with letter Z on even minutes`)
                }
            }
        }
        micrometerRegistry(appMicrometerRegistry)
    }
}

fun myTranslator(rule: String): String {
    return when (rule) {
        ::titleAndBodyCannotStartWithTheSameLetter.name -> "The title and the body must start with different letters"
        else -> defaultTranslator(rule)
    }
}

fun `Everybody can read everything`(
    model: Model<out ModelProperties>,
    context: Context,
    reader: Reader<Views>
): PositiveAuthorization {
    return Allow
}

fun `Everybody can do everything`(
    command: Command<out ModelProperties>,
    context: Context,
    reader: Reader<Views>
): PositiveAuthorization {
    return Allow
}

fun `Zoidberg cannot create todos starting with letter Z on even minutes`(
    command: Command<out ModelProperties>,
    context: Context,
    reader: Reader<Views>
): NegativeAuthorization {
    val modelIdentity = context.actor as? ModelIdentity<User> ?: return Pass
    if (modelIdentity.model.props.name.value != "Zoidberg") {
        return Pass
    }
    if (command.event != TodoEvents.`Create todo`.toEvent()) {
        return Pass
    }
    val commandParams = command.getEventParamsContainer().get<CreateTodoParams>()
    if (!commandParams.title.value.lowercase().startsWith("z")) {
        return Pass
    }
    val isEven = LocalTime.ofInstant(context.time, ZoneId.systemDefault()).minute % 2 == 0
    return if (isEven) Deny else Pass
}
