package com.prettybyte.todo.data.todo

import com.prettybyte.clerk.Event
import com.prettybyte.clerk.Validity
import com.prettybyte.clerk.statemachine.StateMachine
import com.prettybyte.clerk.statemachine.stateMachine
import com.prettybyte.todo.data.`Notify boss that job is done`
import com.prettybyte.todo.data.Views
import com.prettybyte.todo.data.todo.TodoEvents.*
import com.prettybyte.todo.data.todo.TodoStates.Created
import com.prettybyte.todo.data.todo.TodoStates.Done

enum class TodoStates {
    Created,
    Done
}

enum class TodoEvents {
    `Create todo`,
    `Set hours`,
    Update,
    `Mark as done`,
    `Mark as not done`,
    `Delete todo`;

    fun toEvent() = Event(Todo::class.simpleName!!, this.name)
}

fun todoStateMachine(views: Views): StateMachine<Todo, Enum<*>, Views> =
    stateMachine {

        externalEvents {
            eventWithParameters(`Create todo`, CreateTodoParams::class) {
                validateParameters(::titleAndBodyCannotStartWithTheSameLetter)
                validReferences(CreateTodoParams::owner, views.users.all)
            }
            eventWithParameters(`Set hours`, SetHoursParams::class)
            eventWithParameters(Update, UpdateTodoParams::class) {
                validReferences(UpdateTodoParams::owner, views.users.all)
            }
            event(`Mark as done`)
            event(`Mark as not done`)
            event(`Delete todo`)
        }

        voidState {
            onEvent(`Create todo`) {
                createModel(Created, ::createTodo)
            }
        }

        state(Created) {
            onEvent(Update) {
                update(::updateTodo)
            }

            onEvent(`Set hours`) {
                update(::updateHours)
            }

            onEvent(`Mark as done`) {
                guard(::mustHaveHours)
                job(`Notify boss that job is done`())
                transitionTo(Done)
            }
        }

        state(Done) {
            onEvent(`Mark as not done`) {
                transitionTo(Created)
            }

            onEvent(`Delete todo`) {
                delete()
            }
        }

    }

fun titleAndBodyCannotStartWithTheSameLetter(params: CreateTodoParams): Validity =
    if (params.title.value.firstOrNull() == params.body.value.firstOrNull()) Validity.Invalid() else Validity.Valid

