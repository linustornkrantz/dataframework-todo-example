package com.prettybyte.todo.data.todo

import com.prettybyte.clerk.BadRequestProblem
import com.prettybyte.clerk.event.Command
import com.prettybyte.clerk.Model
import com.prettybyte.clerk.Problem
import com.prettybyte.clerk.read.Reader
import com.prettybyte.todo.data.Views

fun createTodo(command: Command<Todo>): Todo {
    val params = command.getEventParamsContainer().get<CreateTodoParams>()
    return Todo(title = params.title, body = params.body, hoursSpent = null, owner = params.owner)
}

fun updateTodo(command: Command<Todo>, model: Model<Todo>): Todo {
    val params = command.getEventParamsContainer().get<UpdateTodoParams>()
    return model.props.copy(title = params.title, body = params.body, owner = params.owner)
}

fun updateHours(command: Command<Todo>, model: Model<Todo>): Todo {
    val params = command.getEventParamsContainer().get<SetHoursParams>()
    return model.props.copy(hoursSpent = params.hours)
}

suspend fun mustHaveHours(model: Model<Todo>?, command: Command<Todo>, reader: Reader<Views>): Problem? {
    requireNotNull(model)
    return if (model.props.hoursSpent == null) BadRequestProblem("hoursSpent must be set") else null
}
