package com.prettybyte.todo.data.todo

import com.prettybyte.clerk.*
import com.prettybyte.clerk.read.Reader
import com.prettybyte.todo.data.Body
import com.prettybyte.todo.data.HoursSpent
import com.prettybyte.todo.data.Title
import com.prettybyte.todo.data.user.User

data class Todo(
    val title: Title,
    val body: Body?,
    val hoursSpent: HoursSpent?,
    val owner: Reference<User>
) : ModelProperties {
    override fun toString() = title.value
}

class CreateTodoParams(val title: Title, val body: Body, val owner: Reference<User>)

typealias UpdateTodoParams = CreateTodoParams

class SetHoursParams(val hours: HoursSpent)
