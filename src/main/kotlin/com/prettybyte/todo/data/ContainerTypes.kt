package com.prettybyte.todo.data

import com.prettybyte.clerk.InvalidParametersProblem
import com.prettybyte.clerk.datatypes.BooleanContainer
import com.prettybyte.clerk.datatypes.EnumContainer
import com.prettybyte.clerk.datatypes.IntContainer
import com.prettybyte.clerk.datatypes.StringContainer

class Title(value: String) : StringContainer(value) {
    override val validLengthMin = 5
    override val validLengthMax = 50
    override val validator: (() -> InvalidParametersProblem?) = ::`Title must start with a capital letter`

    private fun `Title must start with a capital letter`(): InvalidParametersProblem? =
        if (value.first().isLowerCase()) InvalidParametersProblem("Expected a capital letter") else null
}

class Body(value: String) : StringContainer(value) {
    override val validLengthMax = 1000
}

class HoursSpent(value: Int) : IntContainer(value) {
    override val minValue = 0
    override val maxValue = 10
}

class UserName(value: String) : StringContainer(value) {
    override val validLengthMin = 3
    override val validLengthMax = 50
}

class IsAdminValue(value: Boolean) : BooleanContainer(value)

class Language(value: Languages) : EnumContainer<Languages>(value)

enum class Languages {
    Swedish,
    English
}
