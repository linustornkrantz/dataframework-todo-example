package com.prettybyte.todo.data.user

import com.prettybyte.clerk.*
import com.prettybyte.todo.data.IsAdminValue
import com.prettybyte.todo.data.Language
import com.prettybyte.todo.data.UserName

data class User(
    val name: UserName,
    val language: Language,
) : ModelProperties {
    override fun toString(): String {
        return name.toString()
    }
}

class CreateUserParams(val name: UserName, val language: Language, val isAdmin: IsAdminValue)

class ChangeLanguageParams(val language: Language)
