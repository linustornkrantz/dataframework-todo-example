package com.prettybyte.todo.data.user

import com.prettybyte.clerk.Event
import com.prettybyte.clerk.statemachine.StateMachine
import com.prettybyte.clerk.statemachine.stateMachine
import com.prettybyte.todo.data.Views
import com.prettybyte.todo.data.user.UserEvents.*

enum class UserStates {
    Created
}

enum class UserEvents {
    `Create user`,
    `Change language`,
    `Delete user`;

    fun toEvent() = Event(User::class.simpleName!!, this.name)
}

fun userStateMachine(): StateMachine<User, Enum<*>, Views> =
    stateMachine {

        externalEvents {
            eventWithParameters(`Create user`, CreateUserParams::class)
            eventWithParameters(`Change language`, ChangeLanguageParams::class)
            event(`Delete user`)
        }

        voidState {
            onEvent(`Create user`) {
                createModel(UserStates.Created, ::createUser)
            }
        }

        state(UserStates.Created) {
            onEvent(`Change language`) {
                update(::changeLanguage)
            }

            onEvent(`Delete user`) {
                delete()
            }
        }

    }
