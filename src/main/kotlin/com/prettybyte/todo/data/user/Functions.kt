package com.prettybyte.todo.data.user

import com.prettybyte.clerk.event.Command
import com.prettybyte.clerk.Model

fun createUser(command: Command<User>): User {
    val params = command.getEventParamsContainer().get<CreateUserParams>()
    return User(name = params.name, language = params.language)
}

fun changeLanguage(command: Command<User>, model: Model<User>): User {
    val params = command.getEventParamsContainer().get<ChangeLanguageParams>()
    return model.props.copy(language = params.language)
}
