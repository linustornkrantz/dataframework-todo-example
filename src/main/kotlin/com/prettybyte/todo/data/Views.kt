package com.prettybyte.todo.data

import com.prettybyte.clerk.ModelView
import com.prettybyte.todo.data.todo.Todo
import com.prettybyte.todo.data.user.User

data class Views(
    val todos: ModelView<Todo> = ModelView(),
    val users: ModelView<User> = ModelView()
)
