package com.prettybyte.todo

import com.expediagroup.graphql.server.ktor.GraphQL
import com.prettybyte.clerk.*
import com.prettybyte.clerk.graphql.EventMutationService
import com.prettybyte.clerk.graphql.GenericQuery
import com.prettybyte.clerk.storage.Persistence
import com.prettybyte.clerk.storage.SqlPersistence
import com.prettybyte.todo.data.Languages
import com.prettybyte.todo.data.createConfig
import com.prettybyte.todo.data.todo.CreateTodoParams
import com.prettybyte.todo.data.todo.titleAndBodyCannotStartWithTheSameLetter
import com.prettybyte.todo.html.createTodoFormTemplate
import com.prettybyte.todo.html.hoursFormBuilder
import com.prettybyte.todo.html.updateTodoFormBuilder
import com.prettybyte.todo.plugins.configureRouting
import graphql.GraphQLContext
import io.ktor.server.application.*
import io.ktor.server.engine.*
import io.ktor.server.metrics.micrometer.*
import io.ktor.server.netty.*
import io.ktor.server.plugins.compression.*
import io.micrometer.prometheus.PrometheusConfig
import io.micrometer.prometheus.PrometheusMeterRegistry
import mu.KotlinLogging
import org.sqlite.SQLiteDataSource
import java.io.File

val appMicrometerRegistry = PrometheusMeterRegistry(PrometheusConfig.DEFAULT)
val clerk = Clerk.create(createConfig(createPersistence()))
private val log = KotlinLogging.logger {}

suspend fun main() {
    if (System.getenv("DEVELOPMENT_MODE")?.lowercase() != "true") {
        log.warn { "The environment variable DEVELOPMENT_MODE must be true in order to avoid CSRF-token problems when running locally" }
    }

    clerk.meta.start()
    validateHtmlForms()
    if (clerk.meta.modelsCount == 0) {
        createSampleData()
    }
    embeddedServer(Netty, port = 8080, host = "0.0.0.0", module = Application::module)
        .start(wait = true)
}

private fun createPersistence(): Persistence {
    val dbFilePath =
        requireNotNull(System.getenv("DATABASE_PATH")) { "The environment variable 'DATABASE_PATH' must be set" }
    val ds = SQLiteDataSource()
    ds.url = "jdbc:sqlite:$dbFilePath"
    return SqlPersistence(ds)
}

private fun Application.module() {
    install(GraphQL) {
        schema {
            packages = listOf("com.prettybyte.todo.graphql", "com.prettybyte.clerk.graphql")
            queries = listOf(GenericQuery(clerk, ::contextFactory))
            mutations = listOf(EventMutationService(clerk, ::contextFactory))
        }
    }
    configureRouting()
    install(MicrometerMetrics) {
        registry = appMicrometerRegistry
    }
    install(Compression)
}

/**
 * Make sure that the forms contain all required fields corresponding to the event parameters. Better to fail fast...
 */
private fun validateHtmlForms() {
    createTodoFormTemplate.validate()
    updateTodoFormBuilder.validate()
    hoursFormBuilder.validate()
}

/**
 * Creates a Context from a Call. Used in the web application.
 *
 * In a real app we would use a session token or similar to figure out who the user is. Here, we always just use the
 * user Zoidberg.
 */
suspend fun ApplicationCall.context(): Context {
    val user = clerk.read(Context(AuthenticationIdentity)) {
        getFirstWhere(views.users.all) { it.props.name.value == "Zoidberg" }
    }
    val translator = when (user.props.language.value) {
        Languages.Swedish -> ::swedish
        else -> ::english
    }
    return Context(ModelIdentity(user), translator = translator)
}

/**
 * Creates a Context from a GraphQLContext. Used in the GraphQL API.
 *
 * In a real app we would use a session token or similar to figure out who the user is. Here, we always just use the
 * user Zoidberg.
 */
suspend fun contextFactory(graphQLContext: GraphQLContext): Context {
    val user = clerk.read(Context(AuthenticationIdentity)) {
        getFirstWhere(views.users.all) { it.props.name.value == "Zoidberg" }
    }
    return Context(ModelIdentity(user), translator = ::english)
}

fun swedish(rule: String): String {
    return when(rule) {
        ::titleAndBodyCannotStartWithTheSameLetter.name -> "Måste börja på olika bokstäver"
        else -> english(rule)
    }
}

fun english(rule: String): String {
    return when(rule) {
        ::titleAndBodyCannotStartWithTheSameLetter.name -> "The first character in Title and Body must differ"
        else -> defaultTranslator(rule)
    }
}
