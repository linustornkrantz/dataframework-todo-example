package com.prettybyte.todo.data

import com.prettybyte.todo.data.todo.CreateTodoParams
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue


class ConfigKtTest {

    @Test
    fun translator() {
        assertEquals("nu funkar det", myTranslator(CreateTodoParams::titleAndBodyCannotStartWithTheSameLetter))
        val theSet = setOf(CreateTodoParams::titleAndBodyCannotStartWithTheSameLetter)
        theSet.forEach {

            assertEquals("nu funkar det", myTranslator(it))
        }

    }
}
