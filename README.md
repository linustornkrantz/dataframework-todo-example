# Todo app using Dataframework

This is a demo of how Dataframework can be used in a simple application.

Note that this demo ignores several aspects (authentication, security headers etc.) that are important in a real
application.

## Build

You will need a private gitlab token in order to access the dataframework artifact. To create the token:

1. Login to gitlab.
2. Make sure you can access the dataframework repo.
3. Navigate to https://gitlab.com/-/profile/personal_access_tokens
4. Create a new token with the 'api' scope.

To make gradle use the token: create/edit the file gradle.properties located in GRADlE_USER_HOME (e.g. ~/.gradle/) so
that it contains:
```gitLabPrivateToken=<token>```

## Run

The environment variable DATABASE_PATH must be set. If it doesn't point to an existing file, a new database will be
created.

You should also set the environment variable DEVELOPMENT_MODE=true, otherwise you will get a problem when rendering the
forms as they will try to set a secure cookie.

Run the main function in Application.kt. Browse to http://0.0.0.0:8080/
