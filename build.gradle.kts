val slf4jVersion: String by project
val ktor_version: String by project
val kotlin_version: String by project
val logback_version: String by project
val graphql_version: String by project
val prometheus_version: String by project
val dataframework_version: String by project
val kotlin_logging_version: String by project
val sqliteJdbcVersion = "3.44.1.0"

plugins {
    kotlin("jvm") version "1.9.0"
    id("io.ktor.plugin") version "2.3.2"
    id("com.expediagroup.graphql") version "7.0.0-alpha.6"
}

group = "com.prettybyte"
version = "0.0.1"
application {
    mainClass.set("com.prettybyte.todo.ApplicationKt")

    val isDevelopment: Boolean = project.ext.has("development")
    applicationDefaultJvmArgs = listOf("-Dio.ktor.development=$isDevelopment")
}

repositories {
    mavenCentral()
    maven {
        url = uri("https://gitlab.com/api/v4/projects/33843632/packages/maven")
        name = "GitLab"
        credentials(HttpHeaderCredentials::class) {
            name = "Private-Token"
            value = findProperty("gitLabPrivateToken") as String?
        }
        authentication {
            create("header", HttpHeaderAuthentication::class)
        }
    }
}

dependencies {
    implementation("org.slf4j:slf4j-simple:$slf4jVersion")
    implementation("io.github.microutils:kotlin-logging-jvm:$kotlin_logging_version")
    implementation("com.prettybyte:dataframework:$dataframework_version")
    implementation("io.ktor:ktor-server-core-jvm:$ktor_version")
    implementation("io.ktor:ktor-server-netty-jvm:$ktor_version")
    implementation("io.ktor:ktor-server-html-builder:$ktor_version")
    implementation("io.ktor:ktor-server-caching-headers-jvm:$ktor_version")
    implementation("io.ktor:ktor-server-compression-jvm:$ktor_version")
    implementation("io.ktor:ktor-server-metrics-micrometer:$ktor_version")
    implementation("io.ktor:ktor-server-compression:$ktor_version")
    implementation("io.micrometer:micrometer-registry-prometheus:$prometheus_version")
    implementation("com.expediagroup:graphql-kotlin-ktor-server:$graphql_version")
    implementation("org.xerial:sqlite-jdbc:$sqliteJdbcVersion")

    testImplementation("io.ktor:ktor-server-tests-jvm:$ktor_version")
    testImplementation("org.jetbrains.kotlin:kotlin-test-junit:$kotlin_version")
}

tasks.withType<Jar> {
    duplicatesStrategy = DuplicatesStrategy.WARN
    // Otherwise you'll get a "No main manifest attribute" error
    manifest {
        attributes["Main-Class"] = "com.prettybyte.todo.ApplicationKt"
    }


    // To add all of the dependencies otherwise a "NoClassDefFoundError" error
    from(sourceSets.main.get().output)

    dependsOn(configurations.runtimeClasspath)
    from({
        configurations.runtimeClasspath.get().filter { it.name.endsWith("jar") }.map { zipTree(it) }

    })
}

kotlin {
    jvmToolchain(17)
}
